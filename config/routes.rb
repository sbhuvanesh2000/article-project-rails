Rails.application.routes.draw do
  root "articles#index"

  resources :articles do
    resources :comments
    get 'preview', on: :member
    get 'search', on: :collection
    post 'search', to: 'articles#find', on: :collection
  end
end
