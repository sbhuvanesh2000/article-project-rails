class AddIndexToComments < ActiveRecord::Migration[6.1]
  def change
    remove_index :comments, :article_id
    add_index :comments, :article_id
  end
end
