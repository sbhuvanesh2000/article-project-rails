class Article < ApplicationRecord
  has_many :comments, dependent: :destroy

  validates :title, presence: true
  # validates :body, presence: true, length: { minimum: 10 }

  # before_validation :check_before_validation, if: :title?

  # after_save :after_save_method
  # after_commit :after_commit_method

  private

  def check_title_presence
    p 'in check_title_presence'
    :title?
  end

  def after_save_method
    p "after_save_method"
  end

  def after_commit_method
    p "after_commit_method"
  end

  def check_before_validation
    title.upcase!
    if body.length < 10
      self.body = body.ljust(10,'.')
    end
    p 'In check_before_validation'
  end
end
