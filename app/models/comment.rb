class Comment < ApplicationRecord
  belongs_to :article
  scope :findbyuser, ->(id = 1) { where(user_id: id) }
end
